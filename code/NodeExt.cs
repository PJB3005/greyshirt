using Godot;

public static class NodeExt
{
    public static T GetNode<T>(this Node node, string path) where T : Node
    {
        return (T)node.GetNode(path);
    }
}