using Godot;
using System;

public class Disco : Light2D
{
    float accum = 0f;
    public override void _Process(float delta)
    {
        accum += delta * 0.2f;
        if (accum > 1f)
        {
            accum -= 1f;
        }

        // GD.Print(accum);
        SetColor(Color.FromHsv(accum, 0.5f, 0.5f));
    }
}
