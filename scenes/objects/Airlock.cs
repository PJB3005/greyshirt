using Godot;
using System;

public class Airlock : StaticBody2D
{
    [Export]
    public bool Thing = false;
    public bool Opened { get; private set; } = false;
    
    private AnimatedSprite sprite;
    private CollisionShape2D shape;
    private Timer timer;

    public override void _Ready()
    {
        GD.Print(Thing);
        sprite = this.GetNode<AnimatedSprite>("AnimatedSprite");
        sprite.Connect("animation_finished", this, nameof(AnimationEnded));
        timer = this.GetNode<Timer>("CloseTimer");
        timer.Connect("timeout", this, nameof(Close));
        this.GetNode<MovementBumper>("MovementBumper").OnBumped += OnBumped;
        shape = this.GetNode<CollisionShape2D>("CollisionShape2D");
    }

    private void OnBumped(Player player)
    {
        Open();
    }

    public void Open()
    {
        Opened = true;
        sprite.Play("opening");
        timer.Start();
    }

    public void Close()
    {
        Opened = false;
        sprite.Play("closing");
    }

    private void AnimationEnded()
    {
        sprite.Stop();
        shape.Disabled = Opened ? true : false;
        sprite.Animation = Opened ? "open" : "closed";
    }
}
