using Godot;
using System;

public class Player : KinematicBody2D
{
    private AnimatedSprite sprite;

    private Direction direction;
    public Direction Direction
    {
        get => direction;
        set
        {
            if (value == direction)
            {
                return;
            }

            direction = value;
            UpdateIcon();
        }
    }

    public override void _Ready()
    {
        sprite = this.GetNode<AnimatedSprite>("AnimatedSprite");
    }

    [Export]
    public int MoveSpeed = 250;
    [Export]
    public int RunSpeed = 400;

    public override void _Process(float delta)
    {
        var velocity = new Vector2();
        if (Input.IsActionPressed("down"))
        {
            Direction = Direction.Down;
            velocity.y += 1;
        }

        else if (Input.IsActionPressed("up"))
        {
            Direction = Direction.Up;
            velocity.y -= 1;
        }

        if (Input.IsActionPressed("left"))
        {
            Direction = Direction.Left;
            velocity.x -= 1;
        }

        else if (Input.IsActionPressed("right"))
        {
            Direction = Direction.Right;
            velocity.x += 1;
        }

        if (velocity.Length() == 0)
        {
            return;
        }

        var speed = Input.IsActionPressed("run") ? RunSpeed : MoveSpeed;
        velocity = velocity.Normalized() * speed;
        MoveAndSlide(velocity);
        
        // Handle bumping.
        var count = GetSlideCount();
        for (var i = 0; i < count; i++)
        {
            var collision = GetSlideCollision(i);
            var collider = (Node)collision.Collider;
            if (collider.HasNode("MovementBumper"))
            {
                collider.GetNode<MovementBumper>("MovementBumper").OnBumped.Invoke(this);
            }
        }
    }

    public void UpdateIcon()
    {
        string iconState;
        switch (Direction)
        {
            case Direction.Down:
                iconState = "down";
                break;
            case Direction.Up:
                iconState = "up";
                break;
            case Direction.Left:
                iconState = "left";
                break;
            case Direction.Right:
                iconState = "right";
                break;
            default:
                throw new InvalidOperationException();
        }

        sprite.Animation = iconState;
    }
}

public enum Direction
{
    Down,
    Up,
    Right,
    Left
}
