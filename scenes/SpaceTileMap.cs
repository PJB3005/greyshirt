using Godot;
using System;

public class SpaceTileMap : TileMap
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here

        var tileset = GetTileset();
        var rect = GetUsedRect();
        for (var x = (int)rect.Position.x; x < rect.End.x; x++)
        {
            for (var y = (int)rect.Position.y; y < rect.End.y; y++)
            {
                var walltype = IsWall(tileset, x, y); 
                if (walltype == WallType.None)
                {
                    continue;
                }

                var dir = BYONDDir.None;
                if (IsWall(tileset, x, y+1) != WallType.None)
                {
                    dir |= BYONDDir.South;
                }
                if (IsWall(tileset, x, y-1) != WallType.None)
                {
                    dir |= BYONDDir.North;
                }
                if (IsWall(tileset, x+1, y) != WallType.None)
                {
                    dir |= BYONDDir.East;
                }
                if (IsWall(tileset, x-1, y) != WallType.None)
                {
                    dir |= BYONDDir.West;
                }

                if (dir == BYONDDir.None)
                {
                    continue;
                }

                int newtype;
                switch (walltype) {
                    case WallType.Wall:
                        newtype = tileset.FindTileByName($"Wall{(int)dir}");
                        break;
                    case WallType.Reinforced:
                        newtype = tileset.FindTileByName($"RWall{(int)dir}");
                        break;
                    default:
                        throw new InvalidOperationException();
                } 
                SetCell(x, y, newtype);
            }
        }
    }

    private WallType IsWall(TileSet set, int x, int y)
    {
        var type = GetCell(x, y);
        if (type == -1)
        {
            return WallType.None;
        }
        var name = set.TileGetName(type);
        if (name.StartsWith("Wall"))
        {
            return WallType.Wall;
        }
        else if (name.StartsWith("RWall"))
        {
            return WallType.Reinforced;
        }
        else
        {
            return WallType.None;
        }
    }

    private enum WallType
    {
        None,
        Wall,
        Reinforced,
    }

    [Flags]
    private enum BYONDDir : byte
    {
        None = 0,
        North = 1,
        South = 2,
        East = 4,
        West = 8,
    }

//    public override void _Process(float delta)
//    {
//        // Called every frame. Delta is time since last frame.
//        // Update game logic here.
//        
//    }
}
